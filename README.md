# Una tabla HTML y CSS con favoritos o marcadores para página de inicio en navegadores de móviles (Firefox en este caso)

Se trata del código de una página web con una Tabla en HTML con un poco de CSS que he diseñado para utilizarla como página de inicio en Firefox Android

## Necesidad

Llevo usando tiempo #Firefox en #Android y no me gusta que solo se puedan fijar unos 8 accesos directos. Creo que tampoco se puede acceder al config:about. Quizá sí se pueda y no lo veo. Tampoco se puede poner una página de inicio, creo.

## Solución

Lo que he montado es una tabla en HTML que hace las veces de página de inicio, con enlaces y listos. La he situado pegada al bottom de la pantalla por tener los dedos más cerca, y he creado acceso directo a esa web para que salga en el menú de apps del móvil. Aquí pongo a disposición un código limpio y con opciones para que se pueda adaptar según las necesidades o favoritos de cada persona: [paginacontabla.html](https://codeberg.org/plr/Tabla-HTML_para_pagina_de_inicio_en_Firefox_Android/src/branch/main/paginacontabla.html). 

Ese html es el que habría que subir a la web (a un hosting), o incluso tenerlo en el móvil para abrirlo con firefox o cualquier navegador. Yo he creado un acceso directo para que aparezca en el menú de aplicaciones del móvil. Con eso, en vez de acceder a Firefox, se accedería a ese acceso directo que iría directo a la tabla con los favoritos.

## Resultado

Ahora en vez de acceder a Firefox, accedo a esa web desde su icono y es como la página de inicio. En futuras actualizaciones y de manera personal, veré cómo queda añadiendo imágenes/iconos en los enlaces en vez de solo texto. Tal y como lo tengo yo, queda como en la foto: 

![Captura del resultado de la web ya en la web](/Captura_De-Tabla_Creada.png)

